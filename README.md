# 2GIS Test

## Reqs:
- Apache 2.4
- PHP >= 7.1
- Postgresql DB >= 10 with PostGIS Extension

# Setup
## Create DB and set up config
Change credentials in:
```
src/settings.php
```

## Then just restore it from 
```
migration\full_dump.tar
```

## Or...

### Import Postgis additions to db
Import these dumps or create new db with using postgis template
```
migration\postgis.sql
migration\spatial_ref_sys.sql
```

### Install deps
Run:
```
composer install
```

### Create tables structure
Run:
```
php src\cli.php /doctrine orm:schema-tool:create
```

### Fill with test dataset
Run:
```
php src\cli.php /generateTestDataSet
```

# Spec
## firms
### body example
```json
{
    "id": 99990,
    "name": "ООО \"Рога и копыта\" #9999-9",
    "phones": [ 
        {
            "phone": "7383602228899, добавочный 123"
        }
    ],
    "building_id": 9999,
    "rubrics_ids": [
        7,
        9
    ]
}
```
### filters examples
- http://89.223.89.135/firms - all firms
- http://89.223.89.135/firms/5 - view firm with id = 5
- http://89.223.89.135/firms?limit=5&offset=20 - limit and offset (max limit = 100)
- http://89.223.89.135/firms?name=5876 - find by name
- http://89.223.89.135/firms?radius=15.5&x=50&y=50 - find by radius
- http://89.223.89.135/firms?rubric_id=3 - find by rubric
- http://89.223.89.135/firms?rubric_id=3&with_child_rubrics=true - include child rubrics
- http://89.223.89.135/firms?building_id=3 - find by building

## buildings
### body example
```json
{
    "id": 1,
    "firms_ids": [
        1,
        2
    ],
    "address": "ул. Ленина, д. 1",
    "point": {
        "x": 56,
        "y": 92
    }
}
```
### filters examples
- http://89.223.89.135/buildings
- http://89.223.89.135/buildings?limit=5&offset=20 - limit and offset (max limit = 100)