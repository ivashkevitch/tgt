<?php

namespace TwoGis\Cli;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Slim\Container;
use Slim\Http\Response;
use TwoGis\Domain\Buildings\Building;
use TwoGis\Domain\Firms\Firm;
use TwoGis\Domain\Firms\FirmPhone;
use TwoGis\Domain\Firms\FirmRubric;
use TwoGis\Domain\Geo\Point;

class GenerateDataController
{
    /** @var EntityManager */
    private $em;

    public function __construct(Container $c)
    {
        $this->em = $c->get('entityManager');
    }

    public function generateAll(Response $response)
    {
        $this->generateRubrics();
        $this->generateBuildings();

        $buildingsRepo = $this->em->getRepository(Building::class);
        $allBuildings = $buildingsRepo->findAll();

        $this->em->clear();

        $rubricRepo = $this->em->getRepository(FirmRubric::class);
        $allRubrics = $rubricRepo->getChildren();

        $this->generateFirms($allBuildings, $allRubrics);

        return $response->withJson(['Done!']);
    }

    private function generateRubrics()
    {
        $rubrics = [
            'Еда' => [
                'Полуфабрикаты оптом',
                'Мясная продукция',
            ],
            'Автомобили' => [
                'Грузовые',
                'Легковые' => [
                    'Запчасти для подвески',
                    'Шины/Диски',
                ]
            ],
            'Спорт'
        ];

        $this->createRubrics($rubrics);
    }

    private function createRubrics(array $rubrics, FirmRubric $parent = null)
    {
        foreach ($rubrics as $key => $rubric) {
            if (is_array($rubric)) {
                $parentForNew = $this->createRubric($key, $parent);
                $this->createRubrics($rubric, $parentForNew);
            } else {
                $this->createRubric($rubric, $parent);
            }
        }
    }

    private function createRubric(string $name, FirmRubric $parent = null)
    {
        $newRubric = new FirmRubric($name);
        $this->em->persist($newRubric);
        if ($parent !== null) {
            $newRubric->setParent($parent);
        }
        $this->em->flush();
        return $newRubric;
    }

    private function generateBuildings()
    {
        for ($i = 1; $i <= 10000; $i++) {
            $building = new Building(
                sprintf('ул. Ленина, д. %d', $i),
                new Point(rand(0, 100), rand(0, 100))
            );
            $this->em->persist($building);
            $this->em->flush();

            $this->em->clear();
        }
    }

    private function generateFirms(array $allBuildings, array $allRubrics)
    {
        foreach ($allBuildings as $building) {
            $building = $this->em->find(Building::class, $building->getId());

            for ($i = 0; $i < 10; $i++) {
                $rubrics = new ArrayCollection([
                    $allRubrics[rand(0, count($allRubrics) - 1)],
                    $allRubrics[rand(0, count($allRubrics) - 1)]
                ]);
                $firm = new Firm(
                    sprintf('ООО "Рога и копыта" #%d-%d', $building->getId(), $i),
                    $rubrics,
                    $building
                );
                $phone1 = new FirmPhone('7383602228899, добавочный 123');
                $firm->addPhone($phone1);
                $phone2 = new FirmPhone('7383602228000');
                $firm->addPhone($phone2);

                $this->em->persist($phone1);
                $this->em->persist($phone2);
                $this->em->persist($firm);
            }

            $this->em->flush();

            // Stay only rubrics in UnitOfWork
            $this->em->clear(Building::class);
            $this->em->clear(Firm::class);
            $this->em->clear(FirmPhone::class);
        }
    }
}