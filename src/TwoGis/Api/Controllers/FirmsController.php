<?php

namespace TwoGis\Api\Controllers;

use Doctrine\ORM\EntityManager;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;
use TwoGis\Api\Exceptions\ApiNotFoundException;
use TwoGis\Domain\Core\Exceptions\NotFoundException;
use TwoGis\Domain\Firms\Firm;
use TwoGis\Domain\Firms\FirmRepository;

class FirmsController extends AbstractApiController
{
    /** @var EntityManager */
    private $em;

    public function __construct(Container $c)
    {
        parent::__construct($c);
        $this->em = $c->get('entityManager');
    }

    public function view(Request $request, Response $response, array $args)
    {
        /** @var FirmRepository $firmRepo */
        $firmRepo = $this->em->getRepository(Firm::class);
        $firm = $firmRepo->find((int)$args['id']);

        if ($firm === null) {
            throw new ApiNotFoundException('Firm not found');
        }

        return $response->withJson([
            'metadata' => [
                'count' => 1,
                'limit' => 1,
                'offset' => 1
            ],
            'firms' => [$firm->toArray()]
        ]);
    }

    public function find(Request $request, Response $response, array $args)
    {
        $searchCriteria = $this->getSearchCriteria($request);

        /** @var FirmRepository $firmRepo */
        $firmRepo = $this->em->getRepository(Firm::class);

        try {
            $firms = $firmRepo->findByCriteria($searchCriteria, $this->limit, $this->offset);
        } catch (NotFoundException $e) {
            throw new ApiNotFoundException($e->getMessage());
        }

        $firmsAsArray = array_map(function (Firm $firm) {
            return $firm->toArray();
        }, $firms);

        return $response->withJson([
            'metadata' => [
                'count' => $firmRepo->getCountByCriteria($searchCriteria),
                'limit' => $this->limit,
                'offset' => $this->offset
            ],
            'firms' => $firmsAsArray
        ]);
    }

    private function getSearchCriteria(Request $request): array
    {
        $searchCriteria = [];

        $rubricId = $request->getParam('rubric_id', null);
        if ($rubricId !== null) {
            $searchCriteria['rubric_id'] = (int) $rubricId;
            $searchCriteria['with_child_rubrics'] = $request->getParam('with_child_rubrics') === 'true';
        }

        $buildingId = $request->getParam('building_id', null);
        if ($buildingId !== null) {
            $searchCriteria['building_id'] = (int) $buildingId;
        }

        $x = $request->getParam('x', null);
        $y = $request->getParam('x', null);
        $radius = $request->getParam('radius', null);
        if ($x !== null && $y !== null && $radius !== null) {
            $searchCriteria['find_by_radius'] = [
                'x' => (float) $x,
                'y' => (float) $y,
                'radius' => (float) $radius,
            ];
        }

        $name = $request->getParam('name');
        if ($name !== null) {
            $searchCriteria['name'] = (string) $name;
        }

        return $searchCriteria;
    }
}