<?php

namespace TwoGis\Api\Controllers;

use Doctrine\ORM\EntityManager;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;
use TwoGis\Api\Exceptions\ApiNotFoundException;
use TwoGis\Domain\Buildings\Building;
use TwoGis\Domain\Core\Exceptions\NotFoundException;
use TwoGis\Domain\Firms\Firm;
use TwoGis\Domain\Firms\FirmRepository;

class BuildingsController extends AbstractApiController
{
    /** @var EntityManager */
    private $em;

    public function __construct(Container $c)
    {
        parent::__construct($c);
        $this->em = $c->get('entityManager');
    }

    public function find(Request $request, Response $response, array $args)
    {
        $qb = $this->em->createQueryBuilder();
        $buildings = $qb
            ->select('b')
            ->from(Building::class, 'b')
            ->setMaxResults($this->limit)
            ->setFirstResult($this->offset)
            ->getQuery()
            ->getResult();

        $totalCount = $this->em->createQueryBuilder()
            ->select('count(b)')
            ->from(Building::class, 'b')
            ->getQuery()
            ->getSingleScalarResult();


        $buildingsAsArray = array_map(function (Building $building) {
            return $building->toArray();
        }, $buildings);

        return $response->withJson([
            'metadata' => [
                'count' => $totalCount,
                'limit' => $this->limit,
                'offset' => $this->offset
            ],
            'buildings' => $buildingsAsArray
        ]);
    }
}