<?php

namespace TwoGis\Domain\Buildings;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use TwoGis\Domain\Firms\Firm;
use Doctrine\ORM\Mapping as ORM;
use TwoGis\Domain\Geo\Point;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="buildings",
 *     indexes={
 *         @ORM\Index(name="idx_point", columns={"point"}, flags={"spatial"})
 *     }
 * )
 **/
class Building
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="TwoGis\Domain\Firms\Firm",mappedBy="building")
     * @var Firm[]|Collection
     */
    private $firms;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $address;

    /**
     * @ORM\Column(type="geometry", options={"geometry_type"="POINT"})
     */
    private $point;

    /**
     * Building constructor.
     * @param string $address
     * @param Point $point
     */
    public function __construct(string $address, Point $point)
    {
        $this->address = $address;
        $this->setPoint($point);
        $this->firms = new ArrayCollection();
    }

    /**
     * @param Firm $firm
     * @return Building
     */
    public function addFirm(Firm $firm)
    {
        if ($this->firms->contains($firm)) {
            return $this;
        }

        $firm->setBuilding($this);
        $this->firms->add($firm);

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Collection|Firm[]
     */
    public function getFirms(): Collection
    {
        return $this->firms;
    }

    public function setPoint(Point $point): Building
    {
        $this->point = $point->toWKT();
        return $this;
    }

    public function getPoint(): Point
    {
        return Point::createFromWKT($this->point);
    }

    public function toArray(): array
    {
        $firmsIds = array_map(function (Firm $firm) {
            return $firm->getId();
        }, $this->firms->toArray());

        return [
            'id' => $this->id,
            'firms_ids' => $firmsIds,
            'address' => $this->address,
            'point' => $this->getPoint()->toArray()
        ];
    }
}