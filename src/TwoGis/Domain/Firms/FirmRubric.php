<?php

namespace TwoGis\Domain\Firms;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="TwoGis\Domain\Firms\FirmRubricRepository")
 * @ORM\Table(name="firms_rubrics")
 * @Gedmo\Tree(type="nested")
 **/
class FirmRubric
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(name="lft", type="integer")
     */
    private $lft;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(name="lvl", type="integer")
     */
    private $lvl;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(name="rgt", type="integer")
     */
    private $rgt;

    /**
     * @Gedmo\TreeRoot
     * @ORM\ManyToOne(targetEntity="FirmRubric")
     * @ORM\JoinColumn(name="tree_root", referencedColumnName="id", onDelete="CASCADE")
     */
    private $root;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="FirmRubric", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="FirmRubric", mappedBy="parent")
     * @ORM\OrderBy({"lft" = "ASC"})
     * @var FirmRubric[]|Collection
     */
    private $children;

    /**
     * @ORM\ManyToMany(targetEntity="Firm", mappedBy="rubrics")
     * @var Firm[]
     */
    private $firms;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $name;

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->firms = new ArrayCollection();
    }

    public function addFirm(Firm $firm)
    {
        if ($this->firms->contains($firm)) {
            return $this;
        }

        $this->firms->add($firm);
        return $this;
    }

    /**
     * @param FirmRubric $parent
     * @return FirmRubric
     */
    public function setParent(FirmRubric $parent)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}