<?php

namespace TwoGis\Domain\Firms;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="firms_phones")
 **/
class FirmPhone
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Firm")
     * @ORM\JoinColumn(name="firm_id", referencedColumnName="id")
     * @var Firm
     */
    private $firm;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $phone;

    public function __construct(string $phone)
    {
        $this->phone = $phone;
    }

    /**
     * @param Firm $firm
     * @return FirmPhone
     */
    public function setFirm(Firm $firm): FirmPhone
    {
        $this->firm = $firm;
        return $this;
    }

    public function toArray(): array
    {
        return [
            'phone' => $this->phone
        ];
    }
}