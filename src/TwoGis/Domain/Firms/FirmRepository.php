<?php

namespace TwoGis\Domain\Firms;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use TwoGis\Domain\Buildings\Building;
use TwoGis\Domain\Core\Exceptions\NotFoundException;

class FirmRepository extends EntityRepository
{
    /**
     * @param array $searchCriteria
     * @param int $limit
     * @param int $offset
     * @return mixed
     * @throws NotFoundException
     */
    public function findByCriteria(array $searchCriteria, int $limit = 10, int $offset = 0)
    {
        return $this->createQueryBuilderWithCriteria($searchCriteria)
            ->select('f')
            ->orderBy('f.id', 'desc')
            ->setMaxResults($limit === 0 ? null : $limit)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param array $searchCriteria
     * @return mixed
     * @throws NotFoundException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getCountByCriteria(array $searchCriteria)
    {
        return $this->createQueryBuilderWithCriteria($searchCriteria)
            ->select('count(f)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param array $criteria
     * @return QueryBuilder
     * @throws NotFoundException
     */
    private function createQueryBuilderWithCriteria(array $criteria): QueryBuilder
    {
        $qb = $this->getEntityManager()->createQueryBuilder()
            ->from(Firm::class, 'f');

        if (isset($criteria['rubric_id'])) {
            $rubric = $this->getRubric($criteria['rubric_id']);

            $rubricsIds = [$rubric->getId()];

            if (isset($criteria['with_child_rubrics']) && $criteria['with_child_rubrics']) {
                $rubricsIds = array_merge($rubricsIds, $this->getChildRubricsIds($rubric));
            }

            $qb
                ->join('f.rubrics', 'r')
                ->andWhere($qb->expr()->in('r.id', $rubricsIds));
        }

        if (isset($criteria['building_id'])) {
            $building = $this->getBuilding($criteria['building_id']);

            $qb
                ->andWhere('f.building = :building_id')
                ->setParameter(':building_id', $building->getId());
        }

        if (!empty($criteria['find_by_radius'])) {
            $qb
                ->join('f.building', 'b')
                ->andWhere('ST_Distance(b.point, ST_Point(:x, :y)) < :radius')
                ->setParameter(':x', $criteria['find_by_radius']['x'])
                ->setParameter(':y', $criteria['find_by_radius']['y'])
                ->setParameter(':radius', $criteria['find_by_radius']['radius']);
        }

        if (!empty($criteria['name'])) {
            $qb
                ->andWhere('f.name LIKE :firm_name')
                ->setParameter(':firm_name', '%'.$criteria['name'].'%');
        }

        return $qb;
    }

    private function getRubric(int $rubricId): FirmRubric
    {
        $rubric = $this->getEntityManager()->find(FirmRubric::class, $rubricId);
        if ($rubric === null) {
            throw new NotFoundException(sprintf(
                'Rubric with id %d not found',
                $rubricId
            ));
        }
        return $rubric;
    }

    private function getChildRubricsIds(FirmRubric $rubric): array
    {
        /** @var FirmRubricRepository $rubricRepo */
        $rubricRepo = $this->getEntityManager()->getRepository(FirmRubric::class);
        return array_map(function (FirmRubric $rubric) {
            return $rubric->getId();
        }, $rubricRepo->getChildren($rubric));
    }

    private function getBuilding(int $buildingId): Building
    {
        $building = $this->getEntityManager()->find(Building::class, $buildingId);
        if ($building === null) {
            throw new NotFoundException(sprintf(
                'Building with id %d not found',
                $buildingId
            ));
        }
        return $building;
    }
}