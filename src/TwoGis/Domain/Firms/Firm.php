<?php

namespace TwoGis\Domain\Firms;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use TwoGis\Domain\Buildings\Building;
use TwoGis\Domain\Core\Exceptions\InvalidArgumentException;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="TwoGis\Domain\Firms\FirmRepository")
 * @ORM\Table(
 *     name="firms",
 *     indexes={
 *         @ORM\Index(columns={"name"}, flags={"fulltext"})
 *     }
 * )
 **/
class Firm
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @var string
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="FirmPhone", mappedBy="firm")
     * @var FirmPhone[]|Collection
     */
    private $phones;

    /**
     * @ORM\ManyToOne(targetEntity="TwoGis\Domain\Buildings\Building")
     * @ORM\JoinColumn(name="building_id", referencedColumnName="id")
     * @var Building
     */
    private $building;

    /**
     * @ORM\ManyToMany(targetEntity="FirmRubric", inversedBy="firms")
     * @ORM\JoinTable(name="firms_rubrics_references")
     * @var FirmRubric[]|Collection
     */
    private $rubrics;

    public function __construct(string $name, Collection $rubrics, Building $building)
    {
        $this->name = $name;
        $this->phones = new ArrayCollection();
        $this->setRubrics($rubrics);

        $building->addFirm($this);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param Building $building
     * @return Firm
     */
    public function setBuilding(Building $building): Firm
    {
        $this->building = $building;
        return $this;
    }

    /**
     * @return Collection|FirmPhone[]
     */
    public function getPhones(): Collection
    {
        return $this->phones;
    }

    public function addPhone(FirmPhone $phone)
    {
        if ($this->phones->contains($phone)) {
            return $this;
        }

        $phone->setFirm($this);
        $this->phones->add($phone);
        return $this;
    }

    /**
     * @return Collection|FirmRubric[]
     */
    public function getRubrics(): Collection
    {
        return $this->rubrics;
    }

    private function setRubrics(Collection $rubrics)
    {
        if ($rubrics->isEmpty()) {
            throw new InvalidArgumentException('Rubrics must not be empty!');
        }

        $this->rubrics = new ArrayCollection();

        foreach ($rubrics as $rubric) {
            if (!$rubric instanceof FirmRubric) {
                throw new InvalidArgumentException(sprintf(
                    'Element is not instance of %s',
                    FirmRubric::class
                ));
            }

            if (!$this->rubrics->contains($rubric)) {
                $this->rubrics->add($rubric);
            }
        }
    }

    public function toArray(): array
    {
        $phones = array_map(function (FirmPhone $phone) {
            return $phone->toArray();
        }, $this->phones->toArray());

        $rubricsIds = array_map(function (FirmRubric $rubric) {
            return $rubric->getId();
        }, $this->rubrics->toArray());

        return [
            'id' => $this->id,
            'name' => $this->name,
            'phones' => $phones,
            'building_id' => $this->building->getId(),
            'rubrics_ids' => $rubricsIds
        ];
    }
}