<?php

namespace TwoGis\Domain\Geo;

use TwoGis\Domain\Core\Exceptions\DomainException;

class Point
{
    /**
     * @var float
     */
    private $x;

    /**
     * @var float
     */
    private $y;

    public function __construct(float $x, float $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    /**
     * @return float
     */
    public function getX(): float
    {
        return $this->x;
    }

    /**
     * @param float $x
     */
    public function setX(float $x): void
    {
        $this->x = $x;
    }

    /**
     * @return float
     */
    public function getY(): float
    {
        return $this->y;
    }

    /**
     * @param float $y
     */
    public function setY(float $y): void
    {
        $this->y = $y;
    }

    public function toWKT()
    {
        return sprintf('POINT(%f %f)', $this->getX(), $this->getY());
    }

    /**
     * @param string $pointInWKT
     * @return Point
     * @throws DomainException
     */
    public static function createFromWKT(string $pointInWKT)
    {
        preg_match('/POINT\((?P<X>.+) (?P<Y>.+)\)/', $pointInWKT, $matches);
        if (count($matches) !== 5) {
            throw new DomainException('Bad WKT Value for point');
        }
        return new self((float) $matches['X'], (float) $matches['Y']);
    }

    public function toArray()
    {
        return [
            'x' => $this->x,
            'y' => $this->y
        ];
    }
}
