<?php

namespace TwoGis\Domain\Infrastrucure\ORM;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\CachedReader;
use Doctrine\Common\EventManager;
use Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Jsor\Doctrine\PostGIS\Event\ORMSchemaEventSubscriber;
use Jsor\Doctrine\PostGIS\Types\GeometryType;
use Slim\Container;

class EntityManagerFactory
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @return EntityManager
     * @throws \Doctrine\Common\Annotations\AnnotationException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\ORMException
     */
    public function createEntityManager(): EntityManager
    {
        \Doctrine\Common\Annotations\AnnotationRegistry::registerFile(
            __DIR__ . '/../../../../../vendor/doctrine/orm/lib/Doctrine/ORM/Mapping/Driver/DoctrineAnnotations.php'
        );

        $cache = new \Doctrine\Common\Cache\ArrayCache();
        $annotationReader = new AnnotationReader();
        $cachedAnnotationReader = new CachedReader($annotationReader, $cache);

        $driverChain = new MappingDriverChain();
        \Gedmo\DoctrineExtensions::registerAbstractMappingIntoDriverChainORM($driverChain, $cachedAnnotationReader);
        $annotationDriver = new AnnotationDriver($cachedAnnotationReader, $this->container['settings']['doctrine']['paths']);
        $driverChain->addDriver($annotationDriver, 'TwoGis\Domain');

        $config = $this->initConfig($driverChain, $cache);
        $evm = $this->initEvm($cachedAnnotationReader);
        $this->registerTypes();

        return EntityManager::create(
            $this->container['settings']['doctrine']['connection'],
            $config,
            $evm
        );
    }

    /**
     * @param $driverChain
     * @param $cache
     * @return Configuration
     */
    private function initConfig($driverChain, $cache): Configuration
    {
        $config = new Configuration();
        $config->setProxyDir(sys_get_temp_dir());
        $config->setProxyNamespace('Proxy');
        $config->setAutoGenerateProxyClasses($this->container['settings']['doctrine']['isDev']);
        $config->setMetadataDriverImpl($driverChain);
        $config->setMetadataCacheImpl($cache);
        $config->setQueryCacheImpl($cache);

        // PostGIS Functions
        \Jsor\Doctrine\PostGIS\Functions\Configurator::configure($config);
        return $config;
    }

    private function initEvm($cachedAnnotationReader)
    {
        // EventManager
        $evm = new EventManager();

        // Tree
        $treeListener = new \Gedmo\Tree\TreeListener();
        $treeListener->setAnnotationReader($cachedAnnotationReader);
        $evm->addEventSubscriber($treeListener);

        // PostGIS
        $evm->addEventSubscriber(new ORMSchemaEventSubscriber());

        return $evm;
    }

    private function registerTypes()
    {
        if (!\Doctrine\DBAL\Types\Type::hasType('geometry')) {
            \Doctrine\DBAL\Types\Type::addType('geometry', GeometryType::class);
        }
    }
}