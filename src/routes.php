<?php

// Routes
$app->get('/firms', \TwoGis\Api\Controllers\FirmsController::class . ':find');
$app->get('/firms/{id}', \TwoGis\Api\Controllers\FirmsController::class . ':view');

$app->get('/buildings', \TwoGis\Api\Controllers\BuildingsController::class . ':find');

