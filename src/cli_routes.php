<?php

$app->get('/doctrine', function ($request, $response) use ($container, $argv) {
    return (new \TwoGis\Cli\DoctrineController($container))
        ->run($request, $response, $argv);
});

$app->get('/generateTestDataSet', function ($request, $response) use ($container, $argv) {
    return (new \TwoGis\Cli\GenerateDataController($container))
        ->generateAll($response);
});