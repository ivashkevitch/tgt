<?php
return [
    'settings' => [
        'displayErrorDetails' => true,
        'addContentLengthHeader' => false,

        'doctrine' => [
            'connection' => [
                'dbname' => '2gis',
                'user' => 'postgres',
                'password' => 'password',
                'host' => 'localhost',
                'port' => 5432,
                'driver' => 'pdo_pgsql',
                'charset' => 'UTF8'
            ],
            'paths' => [
                __DIR__ . '/TwoGis/Domain',
            ],
            'isDev' => true
        ],
    ],
];
