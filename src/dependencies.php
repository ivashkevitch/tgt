<?php
// DIC configuration

$container = $app->getContainer();

$container['entityManager'] = function ($c) {
    $factory = new \TwoGis\Domain\Infrastrucure\ORM\EntityManagerFactory($c);
    return $factory->createEntityManager();
};

